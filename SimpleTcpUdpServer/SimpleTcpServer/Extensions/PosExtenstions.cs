﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleServer
{
    public static class PosExtenstions
    {
        public static byte[] GetBytes(this string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetString(this byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }

        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        public static T[] Add<T>(this T[] array, T newValue)
        {
            int newLength = array.Length + 1;

            T[] result = new T[newLength];

            Array.Copy(array, 0, result, 0, array.Length);

            result[newLength - 1] = newValue;

            return result;
        }

    }
}
