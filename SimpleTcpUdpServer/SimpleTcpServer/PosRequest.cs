﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleServer
{
    class PosRequest
    {
        public PosRequest(byte[] array)
        {
            //var curPos = 0;
            Length = array.Length;
            Un = array.SubArray(0, 8);
            if (Length > 8)
            {
                    //todo add conditions if bytes absent
                Type = array[8];
                    Sum = array.SubArray(9, 4);
                Currency = array.SubArray(13, 2);
                if (Length > 15)
                {
                    UserData = array.SubArray(15, Length - 15);
                }               
            }
        }

        public byte[] GetSomeResponse()
        {
            byte code = 0x02;
            Random rnd = new Random();
            var response = new byte[1];
            response[0] = code;
            var bankResponse = new byte[12];
            rnd.NextBytes(bankResponse);
            var pan = new byte[19];
            rnd.NextBytes(pan);
            var userData = new byte[100];
            rnd.NextBytes(userData);
            response = response.Concat(Un).Concat(bankResponse).ToArray().Add((byte) 1).Add(Type)
                .Concat(Sum).Concat(Currency).Concat(pan).Concat(userData).ToArray();
            return response;
        }


        public int Length { get; set; }

        public byte[] Un { get; set; }

        public byte Type { get; set; }

        public byte[] Sum { get; set; }

        public byte[] Currency { get; set; }

        public byte[] UserData { get; set; }
    }

    
}
