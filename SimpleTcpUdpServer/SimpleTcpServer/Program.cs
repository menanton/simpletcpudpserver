﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace SimpleServer
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            int recv;
            byte[] data = new byte[1024];
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any,
                                             9050);

            Socket newsock = new
                Socket(AddressFamily.InterNetwork,
                       SocketType.Dgram, ProtocolType.Udp);

            newsock.Bind(ipep);
            Console.WriteLine("Waiting for a client...");

            IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
            EndPoint Remote = (EndPoint) (sender);


            while (true)
            {
                data = new byte[1024];
                recv = newsock.ReceiveFrom(data, ref Remote);

                var request = new PosRequest(data);

                long longVar = BitConverter.ToInt64(request.Un, 0);
                DateTime dateTimeVar = DateTime.FromBinary(longVar);
                Console.WriteLine("request un - " + dateTimeVar.ToString());
               
                var response = request.GetSomeResponse();
                newsock.SendTo(response, response.Length, SocketFlags.None, Remote);
                
            }
        }

        //Для взаимодействия по TCP
        //static void Main(string[] args)
        //{
        //    int recv;
        //    byte[] data = new byte[1024];
        //    IPEndPoint ipep = new IPEndPoint(IPAddress.Any,
        //                           9050);

        //    Socket newsock = new
        //        Socket(AddressFamily.InterNetwork,
        //                    SocketType.Stream, ProtocolType.Tcp);

        //    newsock.Bind(ipep);
        //    newsock.Listen(10);
        //    Console.WriteLine("Waiting for a client...");
        //    Socket client = newsock.Accept();
        //    IPEndPoint clientep =
        //                 (IPEndPoint)client.RemoteEndPoint;
        //    Console.WriteLine("Connected with {0} at port {1}",
        //                    clientep.Address, clientep.Port);


        //    string welcome = "Welcome to my test server";
        //    data = Encoding.ASCII.GetBytes(welcome);
        //    //client.Send(data, data.Length, SocketFlags.None);
        //    while (true)
        //    {
        //        data = new byte[1024];
        //        recv = client.Receive(data);
        //        if (recv == 0)
        //            break;

        //        // Console.WriteLine(Encoding.ASCII.GetString(data, 0, recv));

        //        var request = new PosRequest(data);

        //        long longVar = BitConverter.ToInt64(request.Un, 0);
        //        DateTime dateTimeVar = DateTime.FromBinary(longVar);
        //        Console.WriteLine("request un - " + dateTimeVar.ToString());

        //        //Console.WriteLine("request un - " + GetString(PosRequest(data)));
        //        var response = request.GetSomeResponse();
        //        client.Send(response, response.Length, SocketFlags.None);
        //    }
        //    Console.WriteLine("Disconnected from {0}",
        //                      clientep.Address);
        //    client.Close();
        //    newsock.Close();
        //}
    }
}
